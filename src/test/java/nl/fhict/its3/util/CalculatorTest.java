package nl.fhict.its3.util;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    void basicPlus() {
        int term1 = 1;
        int term2 = 2;
        int expectedValue  =3;
        int actualResult = Calculator.plus(term1, term2);
        assertEquals(expectedValue, actualResult);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sumData.csv", numLinesToSkip = 1)
    void sum_shoudGenerateTheExpectedSumValuesFromCVSFile(
            int term1, int term2, int expected){
        int actualResult = Calculator.plus(term1, term2);
        assertEquals(expected, actualResult);

    }
}